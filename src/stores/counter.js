import { defineStore } from 'pinia'
export const useUserStore = defineStore('user', {
  state: () => ({
    post: [],
    editingPostId: null
  }),
  getters: {
    getPost(state) {
      return state.post
    }
  },
  actions: {
    fetchPost() {
      fetch('https://dummyjson.com/posts')
        .then((response) => {
          if (response.ok) {
            return response.json()
          } else {
            throw new Error('Request failed')
          }
        })
        .then((data) => {
          this.$state.post = data.posts
          console.log(data)
        })
        .catch((error) => {
          console.error(error)
        })
    },
    addPost(data) {
      this.post.push(data)
    },
    incrementReactions(id) {
      console.log(id, 'inc id')
      const foundPost = this.post.find((post) => post.id === id)
      foundPost.reactions++
    },
    editPost(post) {
      const data = {
        title: post.title
      }
      fetch(`https://dummyjson.com/posts/${post.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
    },
    deletePost(id) {
      fetch(`https://dummyjson.com/posts/${id}`, {
        method: 'DELETE'
      })
        .then((res) => res.json())
        .then((deletedData) => {
          console.log('Post deleted:', deletedData)
          this.post = this.post.filter((post) => post.id !== id)
          console.log(this.post, 'deleted')
        })
    },

    multipleDelete(id){

    }
  }
})
